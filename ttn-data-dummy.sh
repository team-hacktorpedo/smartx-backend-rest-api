curl -X POST -H "Content-Type: application/json" -d '{
  "name": "as.up.data.forward",
  "time": "2021-10-30T21:23:58.473039560Z",
  "identifiers": [
    {
      "device_ids": {
        "device_id": "eui-70b3d57ed00477da",
        "application_ids": {
          "application_id": "smartx-backend"
        }
      }
    },
    {
      "device_ids": {
        "device_id": "eui-70b3d57ed00477da",
        "application_ids": {
          "application_id": "smartx-backend"
        },
        "dev_eui": "70B3D57ED00477DA",
        "dev_addr": "260BEC52"
      }
    }
  ],
  "data": {
    "@type": "type.googleapis.com/ttn.lorawan.v3.ApplicationUp",
    "end_device_ids": {
      "device_id": "eui-70b3d57ed00477da",
      "application_ids": {
        "application_id": "smartx-backend"
      },
      "dev_eui": "70B3D57ED00477DA",
      "dev_addr": "260BEC52"
    },
    "correlation_ids": [
      "as:up:01FK9HCYVWKBARK9A6NF39TQ3B",
      "gs:conn:01FK9FFTS3NASFQ2K7TK2S8ACD",
      "gs:up:host:01FK9FFTSE41HZ6FYNEDQFSAR3",
      "gs:uplink:01FK9HCYMPEHNP0FCX98QD6N0A",
      "ns:uplink:01FK9HCYMQSFZGKNZR4H4QEC2Z",
      "rpc:/ttn.lorawan.v3.GsNs/HandleUplink:01FK9HCYMQHYH6TZF0CTGR8HC9",
      "rpc:/ttn.lorawan.v3.NsAs/HandleUplink:01FK9HCYVV5Q5MCJ8NCTX5TGG4"
    ],
    "received_at": "2021-10-30T21:23:58.463310472Z",
    "uplink_message": {
      "f_port": 1,
      "f_cnt": 80,
      "frm_payload": "SGVsbG8gd29ybGQhIFs4MF0=",
      "rx_metadata": [
        {
          "gateway_ids": {
            "gateway_id": "hackathon",
            "eui": "C0EE40FFFF297AA1"
          },
          "timestamp": 2005575555,
          "rssi": -54,
          "channel_rssi": -54,
          "snr": 10,
          "uplink_token": "ChcKFQoJaGFja2F0aG9uEgjA7kD//yl6oRCDz6q8BxoLCO7v9osGEI+J2W0guK/5rK86"
        }
      ],
      "settings": {
        "data_rate": {
          "lora": {
            "bandwidth": 125000,
            "spreading_factor": 7
          }
        },
        "data_rate_index": 5,
        "coding_rate": "4/5",
        "frequency": "868100000",
        "timestamp": 2005575555
      },
      "received_at": "2021-10-30T21:23:58.231590900Z",
      "consumed_airtime": "0.071936s",
      "network_ids": {
        "net_id": "000013",
        "tenant_id": "ttn",
        "cluster_id": "ttn-eu1"
      }
    }
  },
  "correlation_ids": [
    "as:up:01FK9HCYVWKBARK9A6NF39TQ3B",
    "gs:conn:01FK9FFTS3NASFQ2K7TK2S8ACD",
    "gs:up:host:01FK9FFTSE41HZ6FYNEDQFSAR3",
    "gs:uplink:01FK9HCYMPEHNP0FCX98QD6N0A",
    "ns:uplink:01FK9HCYMQSFZGKNZR4H4QEC2Z",
    "rpc:/ttn.lorawan.v3.GsNs/HandleUplink:01FK9HCYMQHYH6TZF0CTGR8HC9",
    "rpc:/ttn.lorawan.v3.NsAs/HandleUplink:01FK9HCYVV5Q5MCJ8NCTX5TGG4"
  ],
  "origin": "ip-10-100-13-103.eu-west-1.compute.internal",
  "context": {
    "tenant-id": "CgN0dG4="
  },
  "visibility": {
    "rights": [
      "RIGHT_APPLICATION_TRAFFIC_READ",
      "RIGHT_APPLICATION_TRAFFIC_READ"
    ]
  },
  "unique_id": "01FK9HCYW9N5GSNED5T90WR5NH"
}' http://marvinfeiter.de:8080/raw 