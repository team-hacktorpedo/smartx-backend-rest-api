from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow, fields
from flask_cors import CORS

import base64

from dotenv import dotenv_values


# load environment values
env_config = dotenv_values("db.env")

# create flask instance, allow access to database
app = Flask(__name__)
CORS(app)

# suppress warning
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# set database connection
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@db:3306/%s' % (
    env_config["MYSQL_USER"], env_config["MYSQL_PASSWORD"], env_config["MYSQL_DATABASE"])

# initialize db
db = SQLAlchemy(app)
ma = Marshmallow(app)


# table configuration for parking locations
class Parking_Location(db.Model):

    # define table, in our case already existing
    __tablename__ = 'parking_location'
    pl_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    gps_location_lat = db.Column(db.Float)
    gps_location_lon = db.Column(db.Float)
    spot_count = db.Column(db.Integer)

    # set class attributes

    def __init__(self, pl_id, name, gps_location_lat, gps_location_lon, spot_count):
        if pl_id:
          self.pl_id = pl_id
        self.name = name
        self.gps_location_lat = gps_location_lat
        self.gps_location_lon = gps_location_lon
        self.spot_count = spot_count

# db schema config for parking locations
class Parking_LocationSchema(ma.Schema):
    class Meta:
        fields = ('pl_id', 'name', 'gps_location_lat',
                  'gps_location_lon', 'spot_count')

# table configuration for parking spots
class Parking_Spot(db.Model):

    # define table, in our case already existing
    __tablename__ = 'parking_spots'
    ps_id = db.Column(db.Integer, primary_key=True)
    pl_id = db.Column(db.Integer)
    occupied = db.Column(db.Boolean)

    # set class atributes
    def __init__(self, ps_id, pl_id, occupied):
      if ps_id:
        self.ps_id = ps_id
      self.pl_id = pl_id
      self.occupied = occupied

# db schema config for parking spots
class Parking_SpotSchema(ma.Schema):
    class Meta:
        fields = ('ps_id', 'pl_id', 'occupied', 'number_plate')



# table configuration for parking locations
class Ttn_Payload(db.Model):

    # define table, in our case already existing
    __tablename__ = 'ttn_payload'
    unique_id = db.Column(db.Integer, primary_key=True)
    payload = db.Column(db.String(255))

    # set class attributes
    def __init__(self, payload):
        self.payload = payload


# db schema config for parking locations
class Ttn_PayloadSchema(ma.Schema):
    class Meta:
        fields = ('unique_id', 'payload')

        
# Init schema
parking_location_schema = Parking_LocationSchema()
parking_locations_schema = Parking_LocationSchema(many=True)
parking_spot_schema = Parking_SpotSchema()
parking_spots_schema = Parking_SpotSchema(many=True)

ttn_payload_schema = Ttn_PayloadSchema()


#define rawData routes
@app.route('/raw', methods=['POST'])
def handle_raw_data():
  """handler for handling raw json rewuests from TTN
  """
  #reference global vars
  global db
  global parking_location_schema

  # Get request data as json
  rawData = request.get_json()

  payload = rawData.get('data').get('uplink_message').get('frm_payload')
  # Url Safe Base64 Decoding
  payloadBytes = base64.urlsafe_b64decode(payload)
  payloadStr = str(payloadBytes, "utf-8")

  new_ttn_payload = Ttn_Payload(payloadStr)

  # add new location object to database and save
  db.session.add(new_ttn_payload)
  # if query invalid, return error description
  try:
    db.session.commit()
  except OperationalError as e:
    return str(e.args[0]+'\n')

  # return JSON of newly created object
  return ttn_payload_schema.jsonify(new_ttn_payload)




#define parking_location routes
@app.route('/parking_location', methods=['POST'])
def add_parking_location(pl_id=None):
  """Insert a new parking_location
  """

  #reference global vars
  global db
  global parking_location_schema

  # Get request data as json
  parking_location_entity = request.get_json()

  # create new locaton object
  new_parking_location = Parking_Location(
      pl_id, 
      parking_location_entity.get('name'), 
      parking_location_entity.get('gps_location_lat'), 
      parking_location_entity.get('gps_location_lon'), 
      parking_location_entity.get('spot_count'))

  # add new location object to database and save
  db.session.add(new_parking_location)
  db.session.commit()

  # return JSON of newly created object
  return parking_location_schema.jsonify(new_parking_location)

@app.route('/parking_location', methods=['GET'])
def get_parking_locations():
  """Retrieve all parking_locations 
  """

  # reference global vars
  global parking_location_schema

  # load all location objects and load to a dict
  result = parking_locations_schema.dump(all_parking_locations)

  # return all fetched objects
  return jsonify(result)

@app.route('/parking_location/<int:pl_id>', methods=['GET'])
def get_parking_location(pl_id):
  """Retrieve a single parking_location
  """
  # reference global vars
  global parking_location_schema

  #load location object with secific id from database
  parking_location = Parking_Location.query.get(pl_id)

  # dump to JSON and return JSON
  result = parking_location_schema.dump(parking_location)
  return parking_location_schema.jsonify(result)

@app.route('/parking_location/<int:pl_id>', methods=['DELETE'])
def delete_parking_location(pl_id):
  """Delete a single parking_location
  """

  # reference global vars
  global db
  global parking_location_schema

  #load location object with secific id from database
  parking_location = Parking_Location.query.get(pl_id)

  # delete his existing object
  db.session.delete(parking_location)
  db.session.commit()

  #return JSON of deleted Object
  return parking_location_schema.jsonify(parking_location)

@app.route('/parking_location/<int:pl_id>', methods=['PUT'])
def update_parking_location(pl_id):
  """Update a single parking_location
  """

  # update location object by deleting and re- adding
  delete_parking_location(pl_id)
  return add_parking_location(pl_id = pl_id)


#define parking_spot routes
@app.route('/parking_spot', methods=['POST'])
def add_parking_spot(ps_id=None):
  """Insert a new parking_spot
  """

  #reference global vars
  global db
  global parking_spot_schema

  # Get request data as json
  parking_spot_entity = request.get_json()

  # create new spot object
  new_parking_spot = Parking_Spot(
    ps_id,  
    parking_spot_entity.get('pl_id'), 
    parking_spot_entity.get('occupied'))

  # add new spot object to database and save
  db.session.add(new_parking_spot)
  db.session.commit()

  # return JSON of newly created object
  return parking_spot_schema.jsonify(new_parking_spot)

@app.route('/parking_spot', methods=['GET'])
def get_parking_spots():
  """Retrieve all parking_spots
  """
  # reference global vars
  global parking_spot_schema

  # load all spot objects and load to a dict
  all_parking_spots = Parking_Spot.query.all()
  result = parking_spots_schema.dump(all_parking_spots)

  # return all fetched objects
  return jsonify(result)

@app.route('/parking_spot/<int:ps_id>', methods=['GET'])
def get_parking_spot(ps_id):
  """Retrieve a single parking_spot
  """
  # reference global vars
  global parking_spot_schema

  #load spot object with secific id from database
  parking_spot = Parking_Spot.query.get(ps_id)
  result = parking_spot_schema.dump(parking_spot)

  # dump to JSON and return JSON
  return jsonify(result)

@app.route('/parking_spot/<int:ps_id>', methods=['DELETE'])
def delete_parking_spot(ps_id):
  """Delete a single parking_spot
  """

  # reference global vars
  global db
  global parking_spot_schema

  #load spot object with secific id from database
  parking_spot = Parking_Spot.query.get(ps_id)

  # delete his existing object
  db.session.delete(parking_spot)
  db.session.commit()

  #return JSON of deleted Object
  return parking_spot_schema.jsonify(parking_spot)


@app.route('/parking_spot/<int:ps_id>', methods=['PUT'])
def update_parking_spot(ps_id):
  """Update a single parking_location
  """
  # update spot object by deleting and re- adding
  delete_parking_spot(ps_id)
  return add_parking_spot(ps_id)


if __name__ == '__main__':  
  # bind service to all net devices of container
  db.create_all()  
  app.run(host="0.0.0.0", port=8080)
