# SmartX backend rest api

## Docker container

Based on the ```python``` image from the docker hub the our image gets generated. The python package requrements get installed as defined in ```docker/requirements.txt```.

The ```docker-compose.yaml``` defines 3 services:
- the python REST api
    - listens for requests on the hosts port ```8080```
    - the >     - the ```api``` folder gets mounted as ```/app/api```
- the mysql database
- a phpmyadmin instance for accessing the database via web GUI

The database credentials are stored in a ```db.env``` which has to be manually created from th ```db.env.sample```

When the container gets started by ```docker-compose up -d``` the api is ready to be used.

The possible requests are defined in ```api/main.py```

## LoRa simulation
The ```ttn-data-dummy.sh``` can be used to simulate a ```POST``` request made by the theThingsNetwork when recieving a loRa package.